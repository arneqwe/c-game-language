#pragma once
#include<graphics.h>
#include<conio.h>
#include<time.h>
#include<Windows.h>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>

#define width 400					//图形库界面的宽度
#define high 600					//图形库界面的高度
#define size 10						//大小
#define blood 6						//血量
#define delayedtime 60				//延时

typedef struct position {			//定义玩家的位置
	int x;							//x轴
	int y;							//y轴
}position;				
typedef struct eposition {			//敌机的相关属性
	double x;						//x轴位置
	double y;						//y轴位置
	int type;						//敌机不同的类型、机型
	double extraspeed;				//敌机的速度
	int life;						//敌机的生命条
}eposition;
typedef struct bposition {			//子弹的相关属性
	double x;						//x轴位置
	double y;						//y轴位置
	int pr;							//攻击方式
	int bullettype;					//项目符号类型
	int enemytype;					//敌人类型
}bposition;
typedef struct rposition {			//残骸的相关属性
	double x;						//x轴位置
	double y;						//y轴位置
	int dietype;					//死亡类型
	int time;						//时间
}rposition;
typedef struct awardposition {		//子弹包的位置
	double x;						//x轴位置
	double y;						//y轴位置
}awardposition;
typedef struct playerhit {			//玩家击中敌机特效
	double x;						//x轴位置
	double y;						//y轴位置
	int delaytime;					//延时时间
}playerhit;
typedef struct boss {				//boss的相关属性
	double x;						//x轴位置
	double y;						//y轴位置
	int attackstatu;				//boss攻击方式
	int eixt;						//boss是否存在
	int isdefeat;					//boss是否被打败
	int curblood;					//现在的血量
	int allblood;					//初始化血量
	int xdirection;					//横向移动方向
	int ydirection;					//纵向移动速度
	int isready;					//boss到位
}boss;

void start();						//初始化
void updatewithinput();				//与输出有关
void updatewithoutinput();			//与输出无关
void show();						//显示画面